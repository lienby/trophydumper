export VITASDK=/usr/local/vitasdk
export PATH=$VITASDK/bin:$PATH


cd kernel/

rm -f cmake_install.cmake
rm -f CMakeCache.txt

cmake .

make install



cd ../user/

rm -f cmake_install.cmake
rm -f CMakeCache.txt

cmake .

make install


cd ../app/

rm -f cmake_install.cmake
rm -f CMakeCache.txt

cmake .

make