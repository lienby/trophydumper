#include <vitasdk.h>
#include <taihen.h>

#include <sys/syslimits.h>

#include <appmgr_user.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#include "graphics.h"


#define printf psvDebugScreenPrintf
#define SetTextColor psvDebugScreenSetFgColor



/* ctrl.c */

void press_exit(void);
int get_key(int type);
int main();




