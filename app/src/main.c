/*
	VitaShell
	Copyright (C) 2015-2016, TheFloW

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "main.h"
#include "ftp.h" // Stolen from old vitashell :D
#include <psp2/net/netctl.h>

char titleid[12];
char g_currentMount[16];
char g_currentPath[PATH_MAX];


int pfsMount(const char *path) {
	char klicensee[0x10];
	SceAppMgrMountIdArgs args;

	memset(klicensee, 0, sizeof(klicensee));

	args.process_titleid = titleid;
	args.path = path;
	args.desired_mount_point = NULL;
	args.klicensee = klicensee;
	args.mount_point = g_currentMount;
        args.id = 0x12F; //trophy_dat0:
	

	return sceAppMgrUserMountById(&args);
}


int WriteFile(char *file, void *buf, int size) {
	SceUID fd = sceIoOpen(file, SCE_O_RDWR | SCE_O_CREAT, 0777);
	if (fd < 0)
		return fd;

	int written = sceIoWrite(fd, buf, size);

	sceIoClose(fd);
	return written;
}

int ReadFile(char *file, void *buf, int size) {
	SceUID fd = sceIoOpen(file,SCE_O_RDONLY, 0777);
	if (fd < 0)
		return fd;

	int read = sceIoRead(fd, buf, size);

	sceIoClose(fd);
	return read;
}

int getFileSize(const char *file) {
	SceUID fd = sceIoOpen(file, SCE_O_RDONLY, 0);
	if (fd < 0)
		return fd;
	int fileSize = sceIoLseek(fd, 0, SCE_SEEK_END);
	sceIoClose(fd);
	return fileSize;
}



void restoreTrophys()
{
    	char kplugin_path[0x200];
	char uplugin_path[0x200];
	sceAppMgrAppParamGetString(0, 12, titleid , 256);

	sprintf(kplugin_path, "ux0:app/%s/module/kernel.skprx", titleid);
	sprintf(uplugin_path, "ux0:app/%s/module/user.suprx", titleid);

	int kernel_modid, user_modid;

	kernel_modid = taiLoadStartKernelModule(kplugin_path, 0, NULL, 0);
	user_modid = sceKernelLoadStartModule(uplugin_path, 0, NULL, 0, NULL, NULL);
        
        psvDebugScreenClear(0);
        int ret;
        ret = sceIoRemove("ur0:/user/00/trophy/data/sce_trop/TRPUSER.DAT");
        psvDebugScreenPrintf("DeleteFile : ur0:/user/00/trophy/data/sce_trop/TRPUSER.DAT ret: 0x%x\n", ret);
        ret = sceIoRemove("ur0:/user/00/trophy/db/trophy_local.db");
        psvDebugScreenPrintf("DeleteFile : ur0:/user/00/trophy/data/trophy_local.db ret: 0x%x\n", ret);
    	char trophy_path[0x200];

	int dfd = sceIoDopen("ur0:user/00/trophy/data/");


	int res;

	do {
		SceIoDirent dir;
		memset(&dir, 0, sizeof(SceIoDirent));

		res = sceIoDread(dfd, &dir);
		if (res > 0 && SCE_S_ISDIR(dir.d_stat.st_mode)) {

                        //trophy procesing begin
                    
			sprintf(trophy_path, "ur0:user/00/trophy/data/%s/", dir.d_name);
                        char path[1024];
                        
                        ret = pfsMount(trophy_path);
                        if (ret == 0 && !strstr(trophy_path,"sce_trop"))
                        {
			psvDebugScreenPrintf("Mounted : %s to %s\n", trophy_path, g_currentMount);
                        
                        
                        //RESTORE TRPTRANS
                        memset(path,0,1024);
                        sprintf(path,"ux0:/trophydump/%s/TRPTRANS.DAT",dir.d_name);
                        
                        
                        //dynamically set size.
                        int size = getFileSize(path);
                        if (size >= 0)
                        {
                        char *TRPTRANS = malloc(size);
                        memset(TRPTRANS,0,size);
                        
                        ret = ReadFile(path,TRPTRANS,size);
                        psvDebugScreenPrintf("ReadFile : %s ret: 0x%x\n", path, ret);
                        
                        memset(path,0,1024);
                        sprintf(path,"%s/TRPTRANS.DAT",g_currentMount);
                        ret = sceIoRemove(path);
                        psvDebugScreenPrintf("DeleteFile : %s ret: 0x%x\n", path, ret);
                        ret = WriteFile(path,TRPTRANS,size);
                        psvDebugScreenPrintf("WriteFile : %s ret: 0x%x\n", path, ret);
                        }
                        
                        //RESTORE TRPTITLE
                        memset(path,0,1024);
                        sprintf(path,"ux0:/trophydump/%s/TRPTITLE.DAT",dir.d_name);
                        
                        
                        //dynamically set size.
                        size = getFileSize(path);
                        if (size >= 0)
                        {
                        char *TRPTITLE = malloc(size);
                        memset(TRPTITLE,0,size);
                        
                        ret = ReadFile(path,TRPTITLE,size);
                        psvDebugScreenPrintf("ReadFile : %s ret: 0x%x\n", path, ret);
                        
                        memset(path,0,1024);
                        sprintf(path,"%s/TRPTITLE.DAT",g_currentMount);
                        ret = sceIoRemove(path);
                        psvDebugScreenPrintf("DeleteFile : %s ret: 0x%x\n", path, ret);
                        ret = WriteFile(path,TRPTITLE,size);
                        psvDebugScreenPrintf("WriteFile : %s ret: 0x%x\n", path, ret);
                        }
                        
                        }
                        else
                        {
                        if (ret != 0)
                        {
                        psvDebugScreenPrintf("Failed to mount: %s ret: 0x%x\n", trophy_path, ret);
                        }
                        }


			sceAppMgrUmount(g_currentMount);
                        psvDebugScreenPrintf("Unmounted : %s\n", trophy_path);



		}
	} while (res > 0);

	sceIoDclose(dfd);
        
        sceKernelStopUnloadModule(user_modid, 0, NULL, 0, NULL, NULL);
	taiStopUnloadKernelModule(kernel_modid, 0, NULL, 0, NULL, NULL); 
        sceKernelDelayThread(1000000);
        main();
}

void ftp()
{
    	char vita_ip[16];
	unsigned short int vita_port;
        sceSysmoduleLoadModule(SCE_SYSMODULE_NET);
        ftp_init(vita_ip,&vita_port);
        psvDebugScreenClear(0);
        psvDebugScreenPrintf("FTP Server running on %s:%i\n",vita_ip,vita_port);
        psvDebugScreenPrintf("Press SQUARE to stop\n");
        sceKernelDelayThread(100000);
        while(1)
        {
            switch(get_key(0)) {
            case SCE_CTRL_SQUARE:
                ftp_fini();
                main();
                break;
            default:
                break;
            }
        }
        
}

void dumpTrophys()
{
    

    
    	char kplugin_path[0x200];
	char uplugin_path[0x200];
	sceAppMgrAppParamGetString(0, 12, titleid , 256);

	sprintf(kplugin_path, "ux0:app/%s/module/kernel.skprx", titleid);
	sprintf(uplugin_path, "ux0:app/%s/module/user.suprx", titleid);

	int kernel_modid, user_modid;

	kernel_modid = taiLoadStartKernelModule(kplugin_path, 0, NULL, 0);
	user_modid = sceKernelLoadStartModule(uplugin_path, 0, NULL, 0, NULL, NULL);
        
        psvDebugScreenClear(0);
        int ret;
        ret = sceIoRemove("ur0:/user/00/trophy/data/sce_trop/TRPUSER.DAT");
        psvDebugScreenPrintf("DeleteFile : ur0:/user/00/trophy/data/sce_trop/TRPUSER.DAT ret: 0x%x\n", ret);
        ret = sceIoRemove("ur0:/user/00/trophy/db/trophy_local.db");
        psvDebugScreenPrintf("DeleteFile : ur0:/user/00/trophy/data/trophy_local.db ret: 0x%x\n", ret);
    	char trophy_path[0x200];

	int dfd = sceIoDopen("ur0:user/00/trophy/data/");


	int res;

	do {
		SceIoDirent dir;
		memset(&dir, 0, sizeof(SceIoDirent));

		res = sceIoDread(dfd, &dir);
		if (res > 0 && SCE_S_ISDIR(dir.d_stat.st_mode)) {

                        //trophy procesing begin
                    
			sprintf(trophy_path, "ur0:user/00/trophy/data/%s/", dir.d_name);
                        char path[1024];
                        
                        ret = pfsMount(trophy_path);
                        if (ret == 0 && !strstr(trophy_path,"sce_trop"))
                        {
			psvDebugScreenPrintf("Mounted : %s to %s\n", trophy_path, g_currentMount);
                        
                        
                        
                        //DUMP TRPTRANS
                        memset(path,0,1024);
                        sprintf(path,"%s/TRPTRANS.DAT",g_currentMount);

                        
                        //dynamically set size.
                        int size = getFileSize(path);
                        if (size >= 0)
                        {
                        char *TRPTRANS = malloc(size);
                        memset(TRPTRANS,0,size);
                        
                        //read file
                        ret = ReadFile(path,TRPTRANS,size);
                        psvDebugScreenPrintf("ReadFile : %s ret: 0x%x\n", path, ret);
                        
                        //make dirs
                        memset(path,0,sizeof(path));
                        sceIoMkdir("ux0:/trophydump", 0006);
                        sprintf(path,"ux0:/trophydump/%s",dir.d_name);
                        sceIoMkdir(path, 0006);
                        memset(path,0,sizeof(path));
                        
                
                        sprintf(path,"ux0:/trophydump/%s/TRPTRANS.DAT",dir.d_name);
                        ret = sceIoRemove(path);
                        psvDebugScreenPrintf("DeleteFile : %s ret: 0x%x\n", path, ret);
                        ret = WriteFile(path,TRPTRANS,size);
                        psvDebugScreenPrintf("WriteFile : %s ret: 0x%x\n", path, ret);
                        }
                                                
                        //DUMP TRPTITLE
                        
                        memset(path,0,1024);
                        sprintf(path,"%s/TRPTITLE.DAT",g_currentMount);

                        
                        //dynamically set size.
                        size = getFileSize(path);
                        if (size >= 0)
                        {
                        char *TRPTITLE = malloc(size);
                        memset(TRPTITLE,0,size);
                        
                        //read file
                        ret = ReadFile(path,TRPTITLE,size);
                        psvDebugScreenPrintf("ReadFile : %s ret: 0x%x\n", path, ret);
                        
                        //make dirs
                        memset(path,0,sizeof(path));
                        sceIoMkdir("ux0:/trophydump", 0006);
                        sprintf(path,"ux0:/trophydump/%s",dir.d_name);
                        sceIoMkdir(path, 0006);
                        memset(path,0,sizeof(path));
                        
                        
                        sprintf(path,"ux0:/trophydump/%s/TRPTITLE.DAT",dir.d_name);
                        ret = sceIoRemove(path);
                        psvDebugScreenPrintf("DeleteFile : %s ret: 0x%x\n", path, ret);
                        ret = WriteFile(path,TRPTITLE,size);
                        psvDebugScreenPrintf("WriteFile : %s ret: 0x%x\n", path, ret);
                        }
                        
                        }
                        else
                        {
                        if (ret != 0)
                        {
                        psvDebugScreenPrintf("Failed to mount: %s ret: 0x%x\n", trophy_path, ret);
                        }
                        }


			sceAppMgrUmount(g_currentMount);
                        psvDebugScreenPrintf("Unmounted : %s\n", trophy_path);



		}
	} while (res > 0);

	sceIoDclose(dfd);
        
        sceKernelStopUnloadModule(user_modid, 0, NULL, 0, NULL, NULL);
	taiStopUnloadKernelModule(kernel_modid, 0, NULL, 0, NULL, NULL); 
        sceKernelDelayThread(1000000);
        main();
}

int main() {
	psvDebugScreenInit();
        psvDebugScreenClear(0);
	sceAppMgrUmount("app0:");

        psvDebugScreenPrintf("--- TrophyDumper ---\n");
        psvDebugScreenPrintf("CROSS: DUMP TROPHYS\n");
        psvDebugScreenPrintf("CIRCLE: RESTORE TROPHYS\n");
        psvDebugScreenPrintf("SQUARE: START FTP\n");
        psvDebugScreenPrintf("TRIANGLE: SYNC TROPHYS\n");
        
        sceKernelDelayThread(100000);
        while(1)
        {
        switch(get_key(0)) {
                case SCE_CTRL_CROSS:
                    dumpTrophys();
                    break;
                case SCE_CTRL_CIRCLE:
                    restoreTrophys();
                    break;
                case SCE_CTRL_SQUARE:
                    ftp();
                    break;
                case SCE_CTRL_TRIANGLE:
                    sceAppMgrLaunchAppByUri(0x20000, "pstc:");
                    break;
                default:
                    break;
                }
        }
        

        
        return 0;
}
