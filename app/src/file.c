/*
	VitaShell
	Copyright (C) 2015-2016, TheFloW

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "main.h"
#include "archive.h"
#include "file.h"

typedef struct {
	char *mount_point;
	char *original_path;
} MountPoint;

static char *mount_points[] = {
	"ux0:",
        "ur0:",
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};

/*
	sa0:
	ux0:app/XXXXXXXXX/
	ux0:cache/XXXXXXXXX/
	ux0:music/
	ux0:picture/
	ux0:pspemu/
	"vs0:data/external"
	"vs0:sys/external"
*/

#define N_MOUNT_POINTS (sizeof(mount_points) / sizeof(char **))

int getPathInfo(char *path, uint32_t *size, uint32_t *folders, uint32_t *files) {
	SceUID dfd = sceIoDopen(path);
	if (dfd >= 0) {
		int res = 0;

		do {
			SceIoDirent dir;
			memset(&dir, 0, sizeof(SceIoDirent));

			res = sceIoDread(dfd, &dir);
			if (res > 0) {
				char *new_path = malloc(strlen(path) + strlen(dir.d_name) + 2);
				sprintf(new_path, "%s/%s", path, dir.d_name);

				getPathInfo(new_path, size, folders, files);

				free(new_path);
			}
		} while (res > 0);

		sceIoDclose(dfd);

		(*folders)++;
	}
	else
	{
		SceIoStat stat;
		memset(&stat, 0, sizeof(SceIoStat));

		int res = sceIoGetstat(path, &stat);
		if (res < 0)
			return res;

		(*size) += stat.st_size;
		(*files)++;
	}

	return 0;
}

int removePath(char *path, uint32_t *value, uint32_t max, void (* SetProgress)(uint32_t value, uint32_t max)) {
	SceUID dfd = sceIoDopen(path);
	if (dfd >= 0) {
		int res = 0;

		do {
			SceIoDirent dir;
			memset(&dir, 0, sizeof(SceIoDirent));

			res = sceIoDread(dfd, &dir);
			if (res > 0) {
				char *new_path = malloc(strlen(path) + strlen(dir.d_name) + 2);
				sprintf(new_path, "%s/%s", path, dir.d_name);
	
				removePath(new_path, value, max, SetProgress);

				free(new_path);
			}
		} while (res > 0);

		sceIoDclose(dfd);

		int ret = sceIoRmdir(path);
		if (ret < 0)
			return ret;

		if (value)
			(*value)++;

		if (SetProgress)
			SetProgress(*value, max);
	} else {
		int ret = sceIoRemove(path);
		if (ret < 0)
			return ret;

		if (value)
			(*value)++;

		if (SetProgress)
			SetProgress(*value, max);
	}

	return 0;
}

int copyPath(char *src, char *dst, uint32_t *value, uint32_t max, void (* SetProgress)(uint32_t value, uint32_t max)) {
	SceUID dfd = sceIoDopen(src);
	if (dfd >= 0) {
		int ret = sceIoMkdir(dst, 0777);
		if (ret < 0 && ret != 0x80010011) {
			sceIoDclose(dfd);
			return ret;
		}

		if (value)
			(*value)++;

		if (SetProgress)
			SetProgress(*value, max);

		int res = 0;

		do {
			SceIoDirent dir;
			memset(&dir, 0, sizeof(SceIoDirent));

			res = sceIoDread(dfd, &dir);
			if (res > 0) {
				char *src_path = malloc(strlen(src) + strlen(dir.d_name) + 2);
				sprintf(src_path, "%s/%s", src, dir.d_name);

				char *dst_path = malloc(strlen(dst) + strlen(dir.d_name) + 2);
				sprintf(dst_path, "%s/%s", dst, dir.d_name);

				copyPath(src_path, dst_path, value, max, SetProgress);

				free(dst_path);
				free(src_path);
			}
		} while (res > 0);

		sceIoDclose(dfd);
	} else {
		SceUID fdsrc = sceIoOpen(src, SCE_O_RDONLY, 0);
		if (fdsrc < 0)
			return fdsrc;

		SceUID fddst = sceIoOpen(dst, SCE_O_WRONLY | SCE_O_CREAT | SCE_O_TRUNC, 0777);
		if (fddst < 0) {
			sceIoClose(fdsrc);
			return fddst;
		}

		void *buf = malloc(TRANSFER_SIZE);

		int read;
		while ((read = sceIoRead(fdsrc, buf, TRANSFER_SIZE)) > 0) {
			sceIoWrite(fddst, buf, read);

			if (value)
				(*value) += read;

			if (SetProgress)
				SetProgress(*value, max);
		}

		free(buf);

		sceIoClose(fddst);
		sceIoClose(fdsrc);
	}

	return 0;
}

int getNumberMountPoints() {
	return N_MOUNT_POINTS;
}

char **getMountPoints() {
	return mount_points;
}

int addMountPoint(char *mount_point) {
	if (mount_point[0] == '\0')
		return 0;

	int i;
	for (i = 0; i < N_MOUNT_POINTS; i++) {
		if (mount_points[i] == NULL) {
			mount_points[i] = mount_point;
			return 1;
		}
	}

	return 0;
}

int replaceMountPoint(char *old_mount_point, char *new_mount_point) {
	int i;
	for (i = 0; i < N_MOUNT_POINTS; i++) {
		if (strcmp(mount_points[i], old_mount_point) == 0) {
			mount_points[i] = new_mount_point;
			return 1;
		}
	}

	return 0;
}


