# TrophyDumper  
1.1: Added Support for 3.68 and 3.69.  
https://bitbucket.org/SilicaAndPina/trophydumper/downloads/TrophyDumper-1.1.vpk  
1.0: Inital Release.  
https://bitbucket.org/SilicaAndPina/trophydumper/downloads/TrophyDumper.vpk  
  
NOTE: After using this app you will receive an error code after running every game
this is because the Trophy_Local.db is invalid simply re-launch the trophy app to fix this!
  
Decrypt/Encrypt Trophy Data Folders at mass.  
  
Features
+ Decrypt ALL Trophys
+ Re-Encrypt ALL Trophys
+ Run an FTP Server (for quick access)
+ Launch Trophy App (for quick access)

Credits to Princess Of Sleeping and TheFlow for helping me with trophy_dat0 mounting :D 
